import pandas as pd
import numpy as np
import pickle
import os.path


compass_directions = {'N': 0,
                      'NNO': 22.5,
                      'NO': 45.0,
                      'ONO': 67.5,
                      'O': 90.0,
                      'OSO':112.5,
                      'SO': 135.0,
                      'SSO':157.5,
                      'S': 180.0,
                      'SSW': 202.5,
                      'SW': 225.0,
                      'WSW': 247.5,
                      'W': 270.0,
                      'WNW': 292.5,
                      'NW': 315.0,
                      'NNW': 337.5}

site_directions = {'Brunsberg': 90.0,
                   'Kella': 135.0,
                   'Börry': 180.0,
                   'Rammi': 315.0
                   }

site_steepness = { 'Rammi': 180.0*np.arctan(200.0/500.0)/np.pi
                 , 'Porta': 180.0*np.arctan(100.0/200.0)/np.pi
                 , 'Brunsberg': 180.0*np.arctan(100.0/200.0)/np.pi
                 , 'Kella': 180.0*np.arctan(100.0/250.0)/np.pi 
                 , 'Dielmissen': 180.0*np.arctan(60./300.0)/np.pi}                  



def get_month_index(meas_date):
    '''
    Gets the integer index of the month based on the measurement time.
        
        Parameters:
            meas_date (int64): An integer corresponing to a measurement time in YYYYMMDDHH format.
    
    '''
    return int((meas_date % 1000000 - meas_date % 10000)/10000)

def compass_aggregtor(phi1, phi2):
    '''
    Helper weight function to map and aggregate the 36 direction values to the 16 compass directions.
    
        Parameters:
            phi1 (float): A degree value (eg from the 16 values in compass_directions)
            phi2 (float): Another degree value (eg from the 36 values in the wind data)
        Returns:
            number (float): The weight contribution from values corresponding to the data point at degree phi2
                            to the aggregation point phi1.
    
    '''
    return (0.5+0.5*np.cos(8.0*(phi1-phi2)) if np.cos(phi1-phi2) > np.cos(np.pi/8.) else 0.0)

def compass_aggregtor_mx():
    '''
    The aggregator organized to a matrix form to simply apply it as a matrix product.    
    '''
    agg_mx = np.zeros((16,36))
    for i in range(16):
        phii = 22.5*i*np.pi/180.0
        for j in range(36):
            phij = 10*(j+1)*np.pi/180.0
            agg_mx[i][j] =compass_aggregtor(phii, phij)
    return agg_mx


def get_wind_dataframe(filename, timeframe_start, timeframe_end):
    '''
    Returns a dataframe containing the filtered historical wind data.
    
        Parameters:
            filename (str): A file name containing the data
            timeframe_start (int64): An integer corresponing to the lower bound of the time frame in YYYYMMDDHH format.
            timeframe_end (int64): An integer corresponing to the upper bound of the time frame in YYYYMMDDHH format.
        Returns:
            df (pandas dataframe): dataframe containing the historical wind data with columns "MESS_DATUM", "strength", and "direction".
    
    '''
    df = pd.read_csv(filename, sep=';')
    df = df.drop(['STATIONS_ID','QN_3','eor'], axis=1).rename(columns={'   F': 'strength', '   D': 'direction'})
    mask = (df['MESS_DATUM'] >= timeframe_start) & (df['MESS_DATUM'] <= timeframe_end) & (df['direction'] > 0 ) #
    df = df[mask]
    
    
    return df

def get_wind_distribution(filename, timeframe_start, timeframe_end, hour):
    '''
    Returns a dataframe of monhtly aggregated historical wind data
       
        Parameters:
            filename (str): A file name containing the data
            timeframe_start (int64): An integer corresponing to the lower bound of the time frame in YYYYMMDDHH format.
            timeframe_end (int64): An integer corresponing to the upper bound of the time frame in YYYYMMDDHH format.
            hour (int): An hour of day to filter
           
        Returns:
            res (pandas dataframe): dataframe containing the aggregated data by month and direction
   
    '''   
    
    df = get_wind_dataframe(filename, timeframe_start, timeframe_end)
    mask =  (df['MESS_DATUM'] % 100 == hour) & (df['strength'] > 0)
    df = df[mask].drop(['strength'],axis=1)
    df['month']=df.apply(lambda row: get_month_index(row['MESS_DATUM']), axis=1)
    df = df.groupby(by=['direction','month']).count().reset_index()
    res = df.pivot(index='direction', columns='month', values='MESS_DATUM').fillna(0)
    
    return res



def airtime_in_minutes(duration_text):
    return int(duration_text.split(':')[0])*60+int(duration_text.split(':')[1])


def flights_by_site(flights_pickle, dur_cutoff=2, flights_cutoff=2):
    '''
    Generates dataframe from pickle file including flights by launch sites and dates.
    
    '''    
    flights = {} #reset
    if os.path.isfile(flights_pickle):
        with open(flights_pickle, 'rb') as handle:
            flights=pickle.load(handle)
            
    df = pd.DataFrame([[site_directions[launch], d.year, d.month, d]  
                for launch in list(site_directions.keys()) # need to map to directions to keep order fixed
                for d in flights[launch]
                for f in flights[launch][d]
                if airtime_in_minutes(f['duration']) > dur_cutoff # filter durations
            ], columns=['launch','year','month','date'])
    df = df.groupby(['launch','year','month','date']).agg(flights=pd.NamedAgg(column='date', aggfunc='count')).reset_index()
    df = df[df['flights'] >= flights_cutoff] # filter by minimum amount of flights
    df = df.groupby(['launch','month']).agg(days=pd.NamedAgg(column='date', aggfunc='count')).reset_index()
    
    return df

def pivoted_flights_by_site(flights_pickle, dur_cutoff=2, flights_cutoff=2):
    '''
    Pivoted data frame for the number of days by months.
    
    '''
    df = flights_by_site(flights_pickle, dur_cutoff, flights_cutoff)
    
    return df.pivot(index='launch', columns='month',values='days').fillna(0)
    

def upwind_component(base_wind_kmh, cross_direction, launch_steepness):
    '''
    Return the upwards wind component for a given base wind (in km/h), devation and launch steepness.
    
        Parameters:
            base_wind_kmh (float): The base wind strength in km/h far away (where it is horisontal)
            cross_direction (float): The deviation of the wind direction from the perpendicular to the launch in degrees
            launch_steepness (float): The steepness of the launch site in 
        
        Returns:
            res (float): The upwards component of the wind in m/s
    '''   
    
    res = (base_wind_kmh/3.6) * np.cos(cross_direction * np.pi/180.0) * np.sin(launch_steepness * np.pi/180.0)
    
    return res    
    

def get_color_code(val, avg):
    if avg == 0:
        return '#ffffff'
    if val >= avg:
        x = val/avg-1.0 
        r = int(np.round(255*(1-x))) 
        g = 255
        b = r
    else:    
        x= val/avg
        r = 255
        g = int(np.round(255*(x)))
        b = g
    return '#{:02x}{:02x}{:02x}'.format( r, g, b)

def get_data_cell(val, avg, roundto):
    return '<td style="background-color: {1};">{0}</td>'.format(
                           str(np.round(val, roundto))
                           , get_color_code(val, avg)
                       )



def get_html_tabular_data(caption, rows, columns, data, roundto=0, hide_avg=False, hide_tot=False):
    '''
    Generates html data table from the tabular data with table caption, row and column headers given by the parameters.
    
        Parameters:
            caption (str): Text description of the data.
            rows (list): A list of strings corresponding to the row headers
            columns (list): A list of strings corresponding to the column headers
            data (list): Tabular data of shape equivalent to rows * columns
         
        Returns:
            html_table (str): Fully rendered <table> element
    
    '''
    #assert len(rows) > 0 , "rows should not be empty"
    #assert len(columns) > 0 , "columns should not be empty"
    #assert len(rows)==np.shape(data)[0] and len(columns)==np.shape(data)[1], "data should have the same shape as rows and columns"
    
    totals = '' 
    if not hide_tot:
        totals='<tr><th>TOT</th>{}<td /></tr>'.format(''.join('<td>{}</td>'.format(str(np.round(t,roundto))) for t in data.sum(axis=0)))
    
    html_table = """<table>
               <caption>{0}</caption>
               <tr><th />{1}{3}</tr>
               <tr>{2}</tr>
               <!-- for checking -->
               {4}
            </table>""".format(
                   caption
                   ,'<th>{0}</th>'.format('</th><th>'.join(c for c in columns))
                   ,'</tr><tr>'.join(
                       '<th>{0}</th>{1}{2}'.format(
                           rows[i]
                           , ''.join(get_data_cell(data[i][j], data[i].mean(), roundto) for j in range(len(columns)))   
                           , '' if hide_avg else '<td style="color: {1}">{0}</td>'.format(str(np.round(data[i].mean(), roundto)),
                                                                            get_color_code(data[i].mean() , data.mean()))
                           ) for i in range(len(rows)))
                   , '' if hide_avg else '<th>AVG</th>'
                   , totals
               )
    
    return html_table


    