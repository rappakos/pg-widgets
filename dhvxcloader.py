import pickle
import os.path
import time
from bs4 import BeautifulSoup
from urllib.request import urlopen
from datetime import timedelta, date, datetime


flights_pickle = './data/dhv.xc.flights.pickle'


dhv_xc_ids = {'Rammi':'9427',
              'Börry':'9403',
              'Kella':'9521',
              'Dielmissen':'9423',
              'Brunsberg':'9844',
              'Porta': '9712',
              'Königszinne': '11489'              
             }

url_pattern = 'https://www.dhv-xc.de/leonardo/index.php?op=list_flights&takeoffID={takeoffID}&year={year}&month={month}&day={day}&pilotID=0&country=0&cat=1'




def load_pickle():
    '''
    Load the flights from the pickle file location.
    
        Returns:
            flights (dict): dictionary of launch site with all previously saved dates. Days without any starts 
    '''
    flights = {} #reset
    if os.path.isfile(flights_pickle):
        with open(flights_pickle, 'rb') as handle:
            flights=pickle.load(handle)
            
    return flights

def validate_flights(flights):
    '''
    Validates the consistency of the flights dictionary.
    
        Returns:
            valid (Boolean): True if no problems detected
    '''

    
    return True
      

def save_pickle(flights):
    '''
    Saves flights with data to pickle file.
    '''
    if validate_flights(flights):
        with open(flights_pickle, 'wb') as handle:
            pickle.dump(flights, handle, protocol=pickle.HIGHEST_PROTOCOL)
    else:
        print('something is wrong with the data - not saved')
    

def get_launch_day(launch, day):
    '''
    Gets the list of flights for a given launch and a given day.
    
        Parameters:
            launch (str): Name of the launch site, should be in the dhv_xc_ids keys.
            day (date): Valid date in the the past
    
        Returns:
            res(list): List of flights with various data
    
    '''
    if launch not in dhv_xc_ids:
        print('Takeoff ID not defined')
        return []
    #if datetime.date(day) >= date.today():
    #    print("Date must be in the past")
    #    return []    
    
    target_url=url_pattern.format(takeoffID=dhv_xc_ids[launch],
                                  year=str(day.year),
                                  month=str(day.month),
                                  day=str(day.day))
    time.sleep(0.2)
    print('fetching', target_url, ' '*20, end='\r')
    res = []
    try:
        soup = BeautifulSoup(urlopen(target_url), 'html.parser')
        table= soup.find('table',{'class':'listTable'})
        for row in table.findChildren('tr')[1:]:
            cols = row.findAll('td')
            # TODO get more properties?
            res.append({
                'date': cols[1].find('div').text, # only for checks?
                'pilot': cols[2].find('a').text,
                'duration': cols[3].text,
                'distance': cols[5].text, 
                'points': cols[6].text,
                'glider': cols[8].find('img')['title'],
                'flight_url': cols[9].find('a', {'class': 'flightLink'})['href'], # TODO
                'max_altitude': '' # TODO
            })            
    except requests.exceptions.Timeout:    
        print("Timeout occurred for", target_url)    
    
    return res
         
            
    
def update(batch_size=10):
    '''
    Downloads and adds a new set of flights
    '''
    print('Start')
    
    flights = load_pickle()
    
    batch = []
    
    for launch in dhv_xc_ids:
        if launch not in flights:
            flights[launch] = {}
        max_date = datetime.strptime('2020-12-31','%Y-%m-%d').date()
        if len(flights[launch]) > 0:
            max_date = min(flights[launch]) # start
        min_date = datetime.strptime('2010-01-01','%Y-%m-%d').date()    
        curr_date=max_date        
        while min_date <= curr_date:
            if curr_date not in flights[launch]:
                batch.append([launch,curr_date])
            curr_date += timedelta(days=-1)
            #print(len(batch))
            if len(batch) >= batch_size:
                break
         
    if len(batch) > 0:
        for t in batch:
            launch = t[0]
            curr_date = t[1]
            res = get_launch_day(launch, curr_date)
            #print(res)
            flights[launch][curr_date] = res
       
    save_pickle(flights)
               
        
        
            
                
    