Various code snippets and data visualisations for paragliding topics.

We do not commit any data for data privacy reasons => "data/" is in the .gitignore

We commit the Jupyter notebooks, but the checkpoints should be kept only locally.

